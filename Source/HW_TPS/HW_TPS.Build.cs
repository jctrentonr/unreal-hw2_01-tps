// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class HW_TPS : ModuleRules
{
	public HW_TPS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "NavigationSystem", "AIModule" });

		PublicIncludePaths.AddRange(new string[]
		{
			"HW_TPS",
			"HW_TPS/Game",
			"HW_TPS/Character",
			"HW_TPS/FuncLibrary"
		});

	}
}
