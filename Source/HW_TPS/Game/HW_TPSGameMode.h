// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HW_TPSGameMode.generated.h"

UCLASS(minimalapi)
class AHW_TPSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHW_TPSGameMode();
};



