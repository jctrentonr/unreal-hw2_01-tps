// Copyright Epic Games, Inc. All Rights Reserved.

#include "HW_TPSGameMode.h"
#include "HW_TPSPlayerController.h"
#include "HW_TPS/Character/HW_TPSCharacter.h"
#include "UObject/ConstructorHelpers.h"

AHW_TPSGameMode::AHW_TPSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AHW_TPSPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}