// Copyright Epic Games, Inc. All Rights Reserved.

#include "HW_TPS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HW_TPS, "HW_TPS" );

DEFINE_LOG_CATEGORY(LogHW_TPS)
 